# Feeds Tamper Bin-to-file
--------------------------

Allows converting binary fields to Drupal file references via feeds tamper.

Originally created by Luke Bainbridge.

## Requirements
---------------

* Tamper (https://www.drupal.org/project/tamper)

## Installation
---------------

Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.
