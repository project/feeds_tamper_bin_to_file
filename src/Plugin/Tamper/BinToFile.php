<?php

namespace Drupal\feeds_tamper_bin_to_file\Plugin\Tamper;

use Drupal\tamper\TamperBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\tamper\TamperableItemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;

/**
 * Plugin implementation for setting a value or default value.
 *
 * @Tamper(
 *   id = "bin_to_file",
 *   label = @Translation("Binary to file"),
 *   description = @Translation("Convert binary data to file"),
 *   category = "Other"
 * )
 */
class BinToFile extends TamperBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['storage_location'] = \Drupal::config('system.file')->get('default_scheme');
    $config['filename'] = '';
    $config['exists_behaviour'] = FileSystemInterface::EXISTS_RENAME;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    // Where should the file be stored?
    $form['storage_location'] = [
      '#type' => 'radios',
      '#title' => $this->t('Storage location'),
      '#description' => $this->t('What file system should the files be saved on?'),
      '#options' => [],
      '#default_value' => isset($this->configuration['storage_location']) ? $this->configuration['storage_location'] : \Drupal::config('system.file')->get('default_scheme'),
    ];
    foreach (\Drupal::service("stream_wrapper_manager")->getWrappers(StreamWrapperInterface::WRITE_VISIBLE) as $wrapper => $info) {
      $form['storage_location']['#options'][$wrapper] = $wrapper;
    }

    // What should the file be named?
    $form['filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filename/location'),
      '#description' => $this->t('What to name the file that is created (you may use replacement patterns).'),
      '#default_value' => isset($this->configuration['filename']) ? $this->configuration['filename'] : '',
      '#required' => TRUE,
    ];

    // How should existing files be handled?
    $form['exists_behaviour'] = [
      '#type' => 'radios',
      '#title' => $this->t('Existing file behaviour'),
      '#description' => $this->t('How should existing files be handled?'),
      '#options' => [
        FileSystemInterface::EXISTS_RENAME => $this->t('Append _{incrementing number} until the filename is unique.'),
        FileSystemInterface::EXISTS_REPLACE => $this->t('Replace the existing file.'),
      ],
      '#default_value' => isset($this->configuration['exists_behaviour']) ? $this->configuration['exists_behaviour'] : FileSystemInterface::EXISTS_RENAME,
    ];

    $replace = [];
    foreach ($this->sourceDefinition->getList() as $source) {
      $replace[] = '[' . $source . ']';
    }

    $form['help'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Available replacement patterns'),
      'list' => [
        '#theme' => 'item_list',
        '#items' => $replace,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->setConfiguration([
      'storage_location' => $form_state->getValue('storage_location'),
      'filename' => $form_state->getValue('filename'),
      'exists_behaviour' => $form_state->getValue('exists_behaviour'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function tamper($data, TamperableItemInterface $item = NULL) {
    if (empty($data)) {
      return;
    }

    // Get current row and build replacements.
    foreach ($item->getSource() as $key => $value) {
      $replacements['[' . $key . ']'] = is_array($value) ? reset($value) : $value;
    }

    // Determine filename and create directory if needed.
    $filename = $this->configuration['storage_location'] . '://' . strtr($this->configuration['filename'], $replacements);
    $directory = dirname($filename);
    if (!is_dir($directory)) {
      \Drupal::service('file_system')->mkdir($directory, NULL, TRUE);
    }

    // Save the file and reset the field to the ID.
    $file = file_save_data($data, $filename, $this->configuration['exists_behaviour']);
    $data = $file->id();

    if (substr($file->getMimeType(), 0, 6) === 'image/') {
      $this->flushImageStyles($filename);
    }

    return $data;
  }

  /**
   * Flush all existing image styles for the given path.
   *
   * @param string $path
   *   The path to the base image.
   */
  protected function flushImageStyles($path) {
    $styles = \Drupal::entityTypeManager()->getStorage('image_style')
      ->loadMultiple();

    foreach ($styles as $style) {
      $style_path = $style->buildUri($path);
      if (is_file($style_path) && file_exists($style_path)) {
        \Drupal::service('file_system')->unlink($style_path);
      }
    }
  }

}
